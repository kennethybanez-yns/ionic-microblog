export  class  Registration {
  email: string;
  username:  string;
  password:  string;
  firstname: string;
  middlename: string;
  lastname: string;
  birthday: string;
}